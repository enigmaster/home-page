# Enigma's homepage

_Home page for Enigma website. The first step to find our content!_

Está página tem como objetivo:
- Ter links para se comunicar com a entidade (Telegram, Email);
- Ter o horário/local das atividades;
- Possuir o material das atividades passadas;

### Instalando

Você irá precisar do pacote `hugo`:

- Arch Linux/Manjaro:

```
sudo pacman -S hugo
```

-  Ubuntu/Debian/Mint/macOS/Windows:

Abra a página `https://github.com/gohugoio/hugo/releases` e baixe a versão EXTENDED (existentes no final da página) referente ao seu sistema operacional.

Após o download execute o arquivo baixado e a instalação estará concluída.

Para verificar utilize o comando `hugo version`:
```
$ hugo version
```
e tenha certeza que a versão instalada corresponde ao seu sistema operacional e à versão extended.

Clone o repositório da página e do tema:

```
git clone https://gitlab.com/enigmaster/home-page
cd home-page
git submodule update --init --recursive
```

Para testar, rode o `hugo server`:

```
$ hugo serve
...
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
...
```

Abra no seu navegador a página `http://localhost:1313/`, e lá estará a página.

Rodando `hugo` sem nenhum parâmetro irá gerar os arquivos HTML e CSS que serão
usados no servidor.

### Adicionando um material

1. Entre na pasta `content/atividades/<ano><semestre>` do ano/semestre em que quer inserir a atividade;

2. Crie um arquivo Markdown (`.md`) com o nome da atividade. No topo do arquivo, um cabeçalho deve conter as seguintes informações:
	```
	---
	title: Título da atividade
	date: data da atividade
	author:  autora da atividade
	draft: false
	tags:
	  - categorias da atividade
	  - categorias da atividade
	---
	```

3. O resto do arquivo deve ser formatado usando a sintáxe do [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet), descrevendo a atividade, bem como links para os arquivos. Os links devem começar com `/material/<ano><semestre>/<nome-da-atividade>/`

4. Para adicionar arquivos, vá até a pasta `/static/material/<ano><semestre>/`. Crie uma pasta com o nome da atividade. Dentro desta pasta, coloque os arquivos que deseja que sejam baixados.

5. Depois de testar sua página, abra um Merge Request no repositório da página
para suas mudanças serem adicionadas à branch master.

6. Depois que o Merge Request for aceito, basta rodar os sequintes comandos no
servidor onde a página está hospedada:

```
cd home-page
git pull
hugo
```

### Reconhecimento

Esse site foi feito usando [Hugo](http://gohugo.io/) com o tema [Hello-friend-ng](https://gitlab.com/enigmaster/hugo-theme-hello-friend-ng).

### Licença

A não ser quando descrito de forma diferente, o conteúdo é licenciado em [Creative Commons (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).

Copyright Enigma. 
