---
title: O que faz um hacker russo em Araraquara
date: 2019-08-08
author: Gabi,junior and Kinder
draft: false
images:
tags:
  - Segurança
  - Hack
  - Vulnerabilidades
---

Nessa atividade, veremos alguns dos hacks mais famosos e as maneiras de como
prevenir esses ataques. Além disso, também veremos boas maneiras de segurança.

{{<figure src="/material/2019s2/o-que-faz-um-hacker-russo-em-araraquara/hacker.jpg"
    position="center" style="width:400px" >}}

## Slides

- [Apresentação](/material/2019s2/o-que-faz-um-hacker-russo-em-araraquara/secomp2k19.pdf)
