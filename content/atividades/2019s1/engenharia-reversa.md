---
title: Introdução a engenharia reversa com GDB
date: 2019-05-21
author: Gabi
draft: false
images:
tags:
  - GDB
  - Engenharia reversa
---

Nessa atividade, veremos como identificar um arquivo binário (executável).
Identificando o executável, veremos algumas ferramentas: tanto para descobrir
informações escondidas no arquivo, como para alterar a sequência de instruções
durante a execução do arquivo.

## Slides

- [Apresentação](/material/2019s1/engenharia-reversa/gdb.pdf)

## Desafios

Tente resolver os desafios! Nos links abaixo, você encontrará alguns
programas executáveis para GNU/Linux x64 e também os códigos-fontes. Tente
acessar as partes restritas!

- [Programas executáveis](/material/2019s1/engenharia-reversa/binaries.zip)
- [Código-fonte dos desafios](/material/2019s1/engenharia-reversa/source.zip)

### Referências

- [Commandos do GDB](/material/2019s1/engenharia-reversa/gdb-refcard.pdf)
