---
title: Criptografia de chave pública 
date: 2019-05-28
author: Teo
draft: false
images:
tags:
  - Criptografia
  - Chave
  - Assimétrica
---

Nessa atividade entenderemos como funciona a criptografia de chave pública, sua utilidade e casos de uso. Através do algoritmo RSA, como exemplo, podemos notar os desafios que uma boa criptografia enfrenta e os frutos que pode dar. 

## Slides

- [Apresentação](/material/2019s1/criptografia-assimetrica/criptografia-assimetrica.pdf)

## Prática

Usando o programa fornecido no link abaixo, siga suas instruções de instalação e execução para obter um simulador de conversa entre um remetente e um destinatário.  
O programa é capaz de enviar uma mensagem de forma que o destinatário consiga ler seu conteúdo e decidir se ele é confiável ou não. Cabe a você enviar a mensagem com a proteção do jeito certo!

- [Simulador (link pro github)](https://github.com/t177398/enigma_rsa)

## Referências

- [Canal do youtube: 3blue1brown - How secure is 256 bit security?](https://www.youtube.com/watch?v=S9JGmA5_unY)

- [Canal do youtube: Computerphile - Key Exchange Problems](https://www.youtube.com/watch?v=vsXMMT2CqqE)

- [Canal do youtube: Computerpihle - Public Key Cryptography](https://www.youtube.com/watch?v=GSIDS_lvRv4)

- [Canal do youtube: Computerphile - Secret Key Exchange (Diffie-Hellman)](https://www.youtube.com/watch?v=NmM9HA2MQGI)

- [Wikipedia - Public Key Cryptography](https://en.wikipedia.org/wiki/Public-key_cryptography)
