---
title: Navegação anônima pela rede Tor
date: 2019-06-18
author: Junior e Andreis
draft: false
images:
tags:
  - Privacidade
  - Tor
---

Nessa atividade, aprenderemos os conceitos básicos por trás da Internet, bem como funciona o rastreamento  de navegação nela. E por fim, apresentaremos a rede Tor, um projeto mantido e defendido por muitos que tem como principal objetivo criar uma rede segura e anónima.

## Slides

- [Apresentação](/material/2019s1/rede-tor/rede-tor.pdf)

## Para Ver Mais

Temos um blogpost com informações complementares sobre a rede Tor. Clique [aqui] (https://enigma.ic.unicamp.br/blog/2018s2/privacidade-tor-pgp/).

Para saber sobre como baixar o Tor e entre outras informações da documentação dele. Clique [aqui] (https://tb-manual.torproject.org/pt-BR/downloading/).
