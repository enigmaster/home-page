---
title: Introdução ao terminal GNU/Linux
date: 2019-02-21
author: Tony
draft: false
images:
tags:
  - GNU/Linux
  - Terminal
---

Nessa atividade, aprenderemos a usar o terminal. Será visto desde os comandos mais simples como: `cd` e `ls`, até comandos mais complexos como escrever a saída de um programa para um arquivo de texto pelo terminal. Aprender comandos no terminal é essencial para se ter uma melhor experiência no GNU/Linux e otimizar a execução de diversas operações como: Mover/apagar vários arquivos utilizando um único comando, usar Git, e entre outros.

## Slides

- [Apresentação](/material/2019s1/guia-terminal/gnulinux_terminal.pdf)

## Prática

Usando o programa fornecido no link abaixo, siga suas intruções de instalação para poderem executar o programa. O objetivo desse exercício é verificar o funcionamento do programa de licença proprietária e tentar corrigi-lo e depois fazer o mesmo com o programa de licença livre. Ao longo disto, você também irá exercitar comandos do terminal.

- [Super Sum Calculator - SSC (link pro GitHub)](https://github.com/LivreCamp/super-sum-calculator)

## Ver Mais

- [Lista e breve definição dos principais comandos do terminal](http://cheatsheetworld.com/programming/unix-linux-cheat-sheet/)
- [Acesse para mais computadores tunados](https://www.reddit.com/r/unixporn)
