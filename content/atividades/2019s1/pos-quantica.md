---
title: A Criptografia na Era Pós-Quântica
date: 2019-06-04
author: Tomas
draft: false
images:
tags:
  - Criptografia
  - Segurança
  - Computação Quântica
---

Nessa atividade, iremos introduzir o funcionamento de um computador quântico; o porquê ele irá quebrar técnicas de Criptografia atuais; e algumas técnicas de cifragem capazes de enfrentar ataques de um futuro computador quântico suficientemente potente.

## Slides

- [Apresentação](/material/2019s1/pos-quantica/criptografia-pos-quantica.pdf)

## Referências

- [What is a quantum computer? Explained with a simple example](https://www.freecodecamp.org/news/what-is-a-quantum-computer-explained-with-a-simple-example-b8f602035365/)

- [What is quantum computing?](https://www.research.ibm.com/ibm-q/learn/what-is-quantum-computing/)

- [Polynomial-Time Algorithms for Prime Factorization and Discrete Logarithms on a Quantum Computer (Artigo Completo - PDF)](https://arxiv.org/pdf/quant-ph/9508027.pdf)

- [Generating hard instances of lattice problems (Artigo Completo)](https://dl.acm.org/citation.cfm?id=237838)
