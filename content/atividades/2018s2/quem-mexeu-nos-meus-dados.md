---
title: Quem mexeu nos meus dados?
date: 2018-08-08
author: Danilo e Tony
draft: false
images:
tags:
  - Segurança
  - Vulnerabilidade
  - Vigilância
---

Nesta atividade iremos mostrar algumas falhas de segurança comuns que podemos encontrar nos nossos dispositivos do dia-a-dia, além de, é claro, mostrar como se defender delas. Tudo foi feito de forma prática, para mostrar quão simples essas vulnerabilidades são. Mostraremos também quais ferramentas ajudam na proteção de dados pessoais em tempos de vigilância e coleta de dados em massa. Com isso, esperamos aumentar a importância que é dada para a segurança digital e despertar o interessse das pessoas, além de apontar caminhos para aqueles que desejam saber mais sobre o assunto.

Essa palestra foi apresentada na Secomp 2018, da forma mais prática possível. Por conta disso, a apresentação não é muito detalhada, mas, contém links e imagens que ajudaram na exposição do assunto.

## Slides

- [Apresentação](/material/2018s2/quem-mexeu-nos-meus-dados/quem-mexeu-nos-meus-dados.pdf)

## Prática

Material complementar feito com o intuito de contextualizar e explicar melhor os ataques e vulnerabilidades expostas na apresentação. Além disso, também contém tutoriais de como realizar esses ataques e/ou testar essas vulnerabilidades, assim como, maneiras para se proteger delas.

- [Realizando ataque Man in the Middle](/material/2018s2/quem-mexeu-nos-meus-dados/man-in-the-middle.pdf)
- [Vulnerabilidade do Android Clipboard](/material/2018s2/quem-mexeu-nos-meus-dados/android-clipboard.pdf)
- [Atacando disco rígido não cifrado](/material/2018s2/quem-mexeu-nos-meus-dados/ataque-disco-rigido.pdf)

