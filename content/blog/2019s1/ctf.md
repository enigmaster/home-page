---
title: Hacker 007 - O desafio de CTF
date: 2019-06-20
author: Junior
draft: false
images:
tags:
  - CTF
  - Vulnerabilidade
  - Decifre.me
  - BugBounty
---

Enquanto um desenvolvedor pensa em como fazer um algoritmo funcionar, um hacker pensa em como fazer aquele algoritmo funcionar de outra forma. E nesse segundo caso, ele obtém outros tipos de resultados. Esse resultado pode ser decriptar uma mensagem, adquirir acesso além do que deveria ou obter uma flag.

Nesse caso, iniciantes no mundo de segurança e criptografia, perguntariam: `Por que eu vou querer obter uma flag?`. Desafios de obtenção de flag, popularmente chamados de CTF (capture the flag), existem para serem desafios para os hackers. Ou seja, esses desafios são as melhores formas do hacker treinar todo o seu conhecimento. Além disso, é melhor do que tentar hackear um site de banco. O banco pode te processar; capturar flags pode aprimorar seu conhecimento, te render reconhecimento, dinheiro e quem sabe até um emprego.

{{< figure src="/blog/2019s1/ctf/hs-ctf.png"
    position="center" style="width:600px" >}}
<center>
Hacker, por Hacker Security
</center>

Espero que tenhamos conseguido despertar sua curiosidade em relação aos CTF's. Pensando nisso, nós desenvolvemos desafios próprios de CTF e uma plataforma única. O nome dela é decifre-me, e pode ser acessada clicando [aqui](https://decifre.me/challenge/home/). Além da nossa plataforma, existem muitas outras plataformas. E todas elas possuem desafios que vão do iniciante até o avançado, e com temas diversos como: web, engenharia reversa, programação, criptografia e entre outros.

Além de plataformas com desafios online, existem as competições e eventos internacionais. São através deles que pode vir o reconhecimento, dinheiro e emprego. Só que antes de tentar entrar nessas competições, é importante acomular uma grande número de CTF's resolvidos e se preparar bem.

## Lista de CTF

- [Decifre-me](https://decifre.me/challenge/home/)
- [Shellter](https://shellterlabs.com/pt/)
- [Easy CTF](https://www.easyctf.com/)
- [Root-me](https://www.root-me.org/?lang=pt)
- [Pico CTF](https://picoctf.com/)
- [The cryptopals crypto challenges](https://cryptopals.com/)
- [Reversing.Kr](http://reversing.kr/)
- [HACKER SECURITY CTF](https://capturetheflag.com.br/)
- [Wargames](https://overthewire.org/wargames/)
- [LMG Network Forensics Puzzle Contest](http://forensicscontest.com/puzzles)
- [Hack This Site](https://www.hackthissite.org/)
- [Practice CTF List](http://captf.com/practice-ctf/)
- [Hack The Box](https://www.hackthebox.eu/)

> Os desafios recomendados para iniciantes são: Easy CTF, Shellter e Root-me.

## Competição/Evento

- [Hacka Flag](https://www.hackaflag.com.br/)
- [Global Cyberlypics](https://www.cyberlympics.org/)
- [Hackers to Hackers Conference - H2HC](https://www.h2hc.com.br/h2hc/pt/)
- [DEFCON](https://www.defcon.org/)
- [Cryptorave](https://cryptorave.org/)


## Bug Bounty

Além disso, existe um tipo de profissional autônomo que desempenha a função de buscar vulnerabilidades. O nome dado a essa profissão é pesquisador de segurança. Seu trabalho é investigar softwares e sites atrás de vulnerabilidades. Essas vulnerabilidades são reportadas como bug bounty, e a empresa, quando quer que pesquisadores busquem falhas em seu software, abre um programa de bug bounty. A recompensa que a empresa dá chama-se bounty. Além disso, também é possível buscar vulnerabilidades, independetemente da empresa abrir um bug bounty, e ainda receber um bounty.

{{< figure src="/blog/2019s1/ctf/BugBounty.png"
    position="center" style="width:200px" >}}
<center>
Bug Bounty, por Enjoy Safer Technology
</center>

Alguns podem se fazer as seguintes perguntas: `O que é um bug bounty?` e `O que isso tem a ver com CTF?`. No CTF, o pesquisador tenta achar alguma brecha no programa (vulnerabilidade), e através dela encontrar a flag. O bug bounty é parecido com isso, ele representa uma vulnerabilidade que permite ao usuário fazer algo que ele não deveria. Ou seja, achar uma flag é achar uma vulnerabilidade e explorá-la.

Com base nisso, assim que um programador descobre uma vulnerabilidade, ele reporta essa vulnerabilidade em sites de Bug Bounty como [Hackerone](https://www.hackerone.com/) ou [openbugbounty](https://www.openbugbounty.org/). Por padrão esses sites reportam a empresa  a vulnerabilidade. Além disso, o pesquisador pode embargar o bug bounty para permitir que a empresa responsável produza um patch de correção, e após o vencimento de período de um tempo, o bug torna-se público.

## Exemplo - ACCESS Segurity Lab vs Banco do Brasil

Esses profissionais podem ser independentes ou trabalharem em grupo. Quando estão em grupo, geralmente criam laborátorios de segurança, que têm a responsabilidade de buscar vulnerabilidades e reporta-las. E em troca, ganham dinheiro com os reports dos Bug Bountys, e se mantêm.

Com esse objetivo existe o [ACCESS Segurity Lab](https://ccess.co/index.html). Um grupo de hackers que investigam diversas vulnerabilidades na web, que além disso, também prestam diversos serviços na área de segurança. Eles já têm inumeros bugs reportados e de grandes organizações como: NASA, Rede Globo, Banco Santander, Netflix e entre outros. Uma das mais comentadas, foi em relação a falha existente no site do Branco do Brasil. Essa falha foi descoberta e reportada, e após um ano, a vulnerabilidade ainda não havia sido corrigida. E com a intenção de forçar o banco a corrigir a falha, eles colocaram Mortal Kombat 3 para rodar (a foto a baixo, é um link para um video). Após o ocorrido, a falha foi mascarada, e dessa vez o ACCESS executou Sonic no site deles. Até que só após esse segundo ataque, o banco removeu a vulnerabilidade.

<center>
[![Banco do Brasil - Mortal Kombat - CCESS](http://img.youtube.com/vi/1bXAhI2o8As/0.jpg)](https://www.youtube.com/watch?time_continue=2&v=1bXAhI2o8As "Banco do Brasil - Mortal Kombat - CCESS")
</center>
